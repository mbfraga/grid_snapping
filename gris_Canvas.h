#pragma once

#include <bitset>

#include <QColor>
#include <QScrollArea>
#include <QWidget>

#include <gris_Snapping.h>

namespace gris {

struct RichRectangle {
  QRect mBoundingBox;
  QColor mColor;
};

//== Canvas ==================================================
/// This class defines a way to ...
class Canvas : public QWidget {
  Q_OBJECT
public:
  /// Constructor
  explicit Canvas(QWidget *parent = nullptr);

  /// Destructor
  virtual ~Canvas() override;

  void addRectangle(const QRect &bb, const QColor &color);

  void paintEvent(QPaintEvent *ev) override;

  QSize sizeHint() const override;

protected:
  void enterEvent(QEvent *ev) override;
  void leaveEvent(QEvent *ev) override;
  void mouseMoveEvent(QMouseEvent *ev) override;
  void mousePressEvent(QMouseEvent *ev) override;
  void mouseReleaseEvent(QMouseEvent *ev) override;

private:
  struct InnerRectangle {
    uint32_t mId = 0;
    RichRectangle mRectangle;
    static constexpr uint32_t k_MouseOver = 1;
    static constexpr uint32_t k_Selected = 2;
    static constexpr uint32_t k_Dragging = 4;
    uint32_t mFlags;
  };

  struct DragData {
    QPoint mDragStart;
    InnerRectangle mOriginalRectangle;
    InnerRectangle mModifiedRectangle;
  };

  void startDrag(const InnerRectangle &target, QPoint pos);
  void resolveDrag();

  static void paintHelperUnderlay(QPainter &painter, const QRect &rect,
                                  const std::vector<InnerRectangle> &rectangles,
                                  const std::optional<DragData> &rectangleDrag,
                                  const SnapMatchData &snapData);

  static void paintHelperOverlay(QPainter &painter, const QRect &rect,
                                 const std::vector<InnerRectangle> &rectangles,
                                 const std::optional<DragData> &rectangleDrag,
                                 const SnapMatchData &snapData);

  static void paintRectangles(QPainter &painter,
                              const std::vector<InnerRectangle> &rectangles,
                              const std::optional<DragData> &rectangleDrag);
  static void
  paintRectangleOverlays(QPainter &painter,
                         const std::vector<InnerRectangle> &rectangles,
                         const std::optional<DragData> &rectangleDrag);

private:
  QMargins mExtra = QMargins(20, 20, 20, 20);
  std::vector<InnerRectangle> mRectangles;
  std::optional<DragData> mRectangleDrag;
  SnapMatchData mDragSnapData;

  uint32_t mRectangleCount = 0;
};

class CanvasContainer : public QScrollArea {
  Q_OBJECT
public:
  /// Constructor
  explicit CanvasContainer(QWidget *parent = nullptr);

  /// Destructor
  virtual ~CanvasContainer() override;

private:
  Canvas *mCanvas = nullptr;
};

} // namespace gris
