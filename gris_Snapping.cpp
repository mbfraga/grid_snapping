#include <gris_Snapping.h>

#include <QPainter>

namespace gris {

namespace Snapping {

SnapGrid filterPotentialSnaps(const QRect &bb, const QRect &fullarea,
                              const std::vector<QRect> &candidates) {
  QRect verticalFilter =
      QRect(QPoint(bb.left(), 0), QSize(bb.width(), fullarea.height()));
  QRect horizontalFilter =
      QRect(QPoint(0, bb.top()), QSize(fullarea.width(), bb.height()));

  std::vector<Snap> verticalSnaps;
  std::vector<Snap> horizontalSnaps;
  SnapList cmpV;
  SnapList cmpH;

  for (const auto &r : candidates) {
    if (!r.isEmpty()) {
      if (verticalFilter.intersects(r)) {
        auto hx = Snapping::horizontalSnapsFromBoundingBox2(r);

        for (auto hit : hx) {
          if (auto found = cmpH.find(hit.mPosition); found != cmpH.end()) {
            found->second.push_back(hit.mMainHit);
            found->second.push_back(hit.mSecondaryHit);

          } else {
            cmpH[hit.mPosition] =
                std::vector<int>{hit.mMainHit, hit.mSecondaryHit};
          }
        }
      }

      if (horizontalFilter.intersects(r)) {
        auto hy = Snapping::verticalSnapsFromBoundingBox2(r);

        for (auto hit : hy) {
          if (auto found = cmpV.find(hit.mPosition); found != cmpV.end()) {
            found->second.push_back(hit.mMainHit);
            found->second.push_back(hit.mSecondaryHit);

          } else {
            cmpV[hit.mPosition] =
                std::vector<int>{hit.mMainHit, hit.mSecondaryHit};
          }
        }
      }
    }
  }

  return SnapGrid{cmpH, cmpV};
}

SnapMatchData snapMatchFromFilteredGrid(const QRect &bb, const SnapGrid &grid,
                                        int sensitivity) {

  int tmpdiff = std::numeric_limits<int>::max();
  SnapMatchData match{grid, {}, {}};

  for (auto c : Snapping::horizontalSnapsFromBoundingBox(bb)) {
    for (auto cand : grid.mHorizontal) {
      if (auto diff = std::abs(cand.first - c); diff < sensitivity) {
        if (diff == 0) {
          match.mHorizontal.mIdealPosition = cand.first;
          match.mHorizontal.mExacts.insert(cand);
          match.mHorizontal.mFuzzys.clear();
          match.mHorizontal.mReferencePosition = c;
          tmpdiff = diff;
        } else if (diff < tmpdiff) {
          match.mHorizontal.mFuzzys.clear();
          match.mHorizontal.mIdealPosition = cand.first;
          match.mHorizontal.mFuzzys.insert(cand);
          match.mHorizontal.mReferencePosition = c;
          tmpdiff = diff;
        }
      }
    }
  }

  tmpdiff = std::numeric_limits<int>::max();
  for (auto c : Snapping::verticalSnapsFromBoundingBox(bb)) {
    for (auto cand : grid.mVertical) {
      if (auto diff = std::abs(cand.first - c); diff < sensitivity) {
        if (diff == 0) {
          match.mVertical.mIdealPosition = cand.first;
          match.mVertical.mExacts.insert(cand);
          match.mVertical.mFuzzys.clear();
          match.mVertical.mReferencePosition = c;
          tmpdiff = diff;
        } else if (diff < tmpdiff) {
          match.mVertical.mFuzzys.clear();
          match.mVertical.mIdealPosition = cand.first;
          match.mVertical.mFuzzys.insert(cand);
          match.mVertical.mReferencePosition = c;
          tmpdiff = diff;
        }
      }
    }
  }

  return match;
}

SnapMatchData snapMatchFromBoundingBox(const QRect &bb, const QRect &fullarea,
                                       const std::vector<QRect> &candidates,
                                       int sensitivity) {
  QRect rect =
      bb.adjusted(-sensitivity, -sensitivity, sensitivity, sensitivity);
  return snapMatchFromFilteredGrid(
      bb, filterPotentialSnaps(rect, fullarea, candidates), sensitivity);
}

//======== Draw Methods ========================================================

void drawHorizontalSnapList(QPainter &painter, const QRect &fullarea,
                            const SnapList &snaplist, const QColor &color,
                            double lineWidth, double dotsize) {
  auto verticalLine =
      QRectF(QPointF(0, 0), QSizeF(lineWidth, fullarea.height()));
  auto dot = QRectF(QPointF(0, 0), QSizeF(dotsize, dotsize));

  for (auto snap : snaplist) {
    verticalLine.moveCenter(QPointF(snap.first, verticalLine.center().y()));
    painter.fillRect(verticalLine, color);

    for (auto hit : snap.second) {
      if (hit != 0) {
        dot.moveCenter(QPointF(snap.first, hit));
        painter.fillRect(dot, color);
      }
    }
  }
}

void drawVerticalSnapList(QPainter &painter, const QRect &fullarea,
                          const SnapList &snaplist, const QColor &color,
                          double lineWidth, double dotsize) {
  auto horizontalLine =
      QRectF(QPointF(0, 0), QSizeF(fullarea.width(), lineWidth));
  auto dot = QRectF(QPointF(0, 0), QSizeF(dotsize, dotsize));

  for (auto vy : snaplist) {
    horizontalLine.moveCenter(QPointF(horizontalLine.center().x(), vy.first));
    painter.fillRect(horizontalLine, color);
    for (auto hit : vy.second) {
      if (hit != 0) {
        dot.moveCenter(QPointF(hit, vy.first));
        painter.fillRect(dot, color);
      }
    }
  }
}

} // namespace Snapping

} // namespace gris
