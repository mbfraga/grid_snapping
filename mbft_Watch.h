#pragma once

// Author: Martin B. Fraga

#pragma once

#include <chrono>

namespace mbft {

// A simple stopwatch.
// Run start() to mark starting point, stop() to mark stop point.
// Use duration() for the duration from mStart to mStop points.
// Use ellapsed_in_*() for specialized counts of duration().
struct Watch
{
  void start();
  void stop();

  template<class DurationT>
  DurationT duration() const {
    return std::chrono::duration_cast<DurationT>(mStop - mStart);
  }

  int ellapsed_in_milliseconds() const;
  int ellapsed_in_seconds() const;

private:
  using TP = std::chrono::time_point<std::chrono::system_clock>;

  TP mStart;
  TP mStop;
};

// Implementation

inline void Watch::start()
{
  mStart = std::chrono::system_clock::now();
}

inline void Watch::stop()
{
  mStop = std::chrono::system_clock::now();
}

inline int Watch::ellapsed_in_milliseconds() const
{
  return duration<std::chrono::milliseconds>().count();
}

inline int Watch::ellapsed_in_seconds() const
{
  return duration<std::chrono::seconds>().count();
}

}


