#pragma once

#include <unordered_map>
#include <vector>

#include <QRect>

class QPainter;
class QColor;

namespace gris {

struct Snap {
  int mPosition = 0;
  int mMainHit = 0;
  int mSecondaryHit = 0;
};

using SnapList = std::unordered_map<int, std::vector<int>>;
struct SnapGrid {
  SnapList mHorizontal;
  SnapList mVertical;
};

struct SnapMatch {
  int mIdealPosition = -1;
  int mReferencePosition = -1;
  SnapList mFuzzys;
  SnapList mExacts;
};

struct SnapMatchData {
  SnapGrid mSnapGrid;
  SnapMatch mHorizontal;
  SnapMatch mVertical;
};

//== Snapping ==================================================
namespace Snapping {

inline std::vector<int> verticalSnapsFromBoundingBox(const QRect &bb) {
  return {bb.top(), bb.center().y(), bb.bottom()};
}

inline std::vector<int> horizontalSnapsFromBoundingBox(const QRect &bb) {
  return {bb.left(), bb.center().x(), bb.right()};
}

inline std::vector<Snap> verticalSnapsFromBoundingBox2(const QRect &bb) {
  return std::vector<Snap>{Snap{bb.top(), bb.left(), bb.right()},
                           Snap{bb.center().y(), bb.center().x()},
                           Snap{bb.bottom(), bb.left(), bb.right()}};
}

inline std::vector<Snap> horizontalSnapsFromBoundingBox2(const QRect &bb) {
  return std::vector<Snap>{Snap{bb.left(), bb.top(), bb.bottom()},
                           Snap{bb.center().x(), bb.center().y()},
                           Snap{bb.right(), bb.top(), bb.bottom()}};
}

SnapGrid filterPotentialSnaps(const QRect &bb, const QRect &fullarea,
                              const std::vector<QRect> &candidates);

SnapMatchData snapMatchFromFilteredGrid(const QRect &bb, const SnapGrid &grid,
                                        int sensitivity);

SnapMatchData snapMatchFromBoundingBox(const QRect &bb, const QRect &fullarea,
                                       const std::vector<QRect> &candidates,
                                       int sensitivity);

void drawHorizontalSnapList(QPainter &painter, const QRect &fullarea,
                            const SnapList &snaplist, const QColor &color,
                            double lineWidth, double dotsize);

void drawVerticalSnapList(QPainter &painter, const QRect &fullarea,
                          const SnapList &snaplist, const QColor &color,
                          double lineWidth, double dotsize);

} // namespace Snapping

} // namespace gris
