#include <mainwindow.h>

#include <gris_Canvas.h>

MainWindow::MainWindow(QWidget* parent)
    : QMainWindow(parent)
{
  setCentralWidget(new gris::CanvasContainer(this));
}

MainWindow::~MainWindow() {}
