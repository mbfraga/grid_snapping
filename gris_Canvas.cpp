#include <gris_Canvas.h>

#include <cmath>
#include <iostream>

#include <QMouseEvent>
#include <QPainter>

#include <gris_Snapping.h>

#include <mbft_Watch.h>

namespace gris {

namespace {

static constexpr int NumOfRectangles = 3;
static constexpr int Distribution = 400;
static constexpr int Sensitivity = 5;
static constexpr bool DrawAllCloseGuides = false;
static constexpr bool DrawMatchedGuides = true;

inline bool setFlagState(uint32_t &flags, uint32_t flag, bool state) {
  bool prev = (flags & flag);
  if (prev == state) {
    return false;
  }

  if (state) {
    flags |= flag;
    return true;
  }

  flags &= ~flag;
  return true;
}

} // namespace

//== Canvas ==================================================

Canvas::Canvas(QWidget *parent) : QWidget(parent) {
  setAttribute(Qt::WA_Hover, true);

  for (auto i = 0; i < NumOfRectangles; ++i) {
    auto x = rand() % Distribution;
    auto y = rand() % Distribution;
    auto w = 50 + rand() % 200;
    auto h = 50 + rand() % 200;
    addRectangle(QRect(QPoint(x, y), QSize(w, h)), QColor(100, 100, 100, 100));
  }
}

Canvas::~Canvas() {}

void Canvas::addRectangle(const QRect &bb, const QColor &color) {
  InnerRectangle r = {++mRectangleCount, {bb, color}, 0};
  mRectangles.push_back(std::move(r));
}

void Canvas::paintEvent(QPaintEvent *ev) {
  auto painter = QPainter(this);

  painter.fillRect(rect(), QColor(180, 180, 180, 255));

  painter.translate(mExtra.left(), mExtra.top());
  paintHelperUnderlay(painter, rect(), mRectangles, mRectangleDrag,
                      mDragSnapData);
  paintRectangles(painter, mRectangles, mRectangleDrag);
  paintRectangleOverlays(painter, mRectangles, mRectangleDrag);
  paintHelperOverlay(painter, rect(), mRectangles, mRectangleDrag,
                     mDragSnapData);
}

QSize Canvas::sizeHint() const {
  auto r = QRect(0, 0, 100000, 100000);

  return r.marginsAdded(mExtra).size();
}

void Canvas::enterEvent(QEvent *ev) { setMouseTracking(true); }

void Canvas::leaveEvent(QEvent *ev) {
  setMouseTracking(false);
  bool needsUpdate = false;
  for (auto &r : mRectangles) {
    needsUpdate = setFlagState(r.mFlags, InnerRectangle::k_MouseOver, false) ||
                  needsUpdate;
  }

  if (needsUpdate) {
    update();
  }
}

void Canvas::mouseMoveEvent(QMouseEvent *ev) {
  if (mRectangleDrag.has_value()) {
    if ((ev->buttons() & Qt::LeftButton)) {
      auto delta = ev->pos() - mRectangleDrag->mDragStart;
      auto r = mRectangleDrag->mOriginalRectangle.mRectangle.mBoundingBox;
      auto newPos = r.topLeft() + delta;
      newPos.rx() = std::max(0, newPos.x());
      newPos.ry() = std::max(0, newPos.y());
      r.moveTopLeft(newPos);

      std::vector<QRect> candidates;
      std::transform(mRectangles.begin(), mRectangles.end(),
                     std::back_inserter(candidates),
                     [](const Canvas::InnerRectangle &ir) {
                       return (ir.mFlags & Canvas::InnerRectangle::k_Dragging)
                                  ? QRect()
                                  : ir.mRectangle.mBoundingBox;
                     });

      mDragSnapData = Snapping::snapMatchFromBoundingBox(r, rect(), candidates,
                                                         Sensitivity);

      bool modified = false;
      if (mDragSnapData.mHorizontal.mIdealPosition >= 0) {
        r.translate(mDragSnapData.mHorizontal.mIdealPosition -
                        mDragSnapData.mHorizontal.mReferencePosition,
                    0);
        modified = true;
      }
      if (mDragSnapData.mVertical.mIdealPosition >= 0) {
        r.translate(0, mDragSnapData.mVertical.mIdealPosition -
                           mDragSnapData.mVertical.mReferencePosition);
        modified = true;
      }

      mDragSnapData = Snapping::snapMatchFromBoundingBox(r, rect(), candidates,
                                                         Sensitivity);

      mRectangleDrag->mModifiedRectangle.mRectangle.mBoundingBox = r;

      update();
    }
    return;
  }

  bool needsUpdate = false;
  bool oneSelected = false;
  auto offsetPos = ev->pos() - QPoint(mExtra.left(), mExtra.top());
  for (auto &r : mRectangles) {
    bool currentSelected =
        oneSelected || r.mRectangle.mBoundingBox.contains(offsetPos);
    needsUpdate = setFlagState(r.mFlags, InnerRectangle::k_MouseOver,
                               !oneSelected && currentSelected) ||
                  needsUpdate;

    oneSelected = oneSelected || currentSelected;
  }

  if (needsUpdate) {
    update();
  }
}

void Canvas::mousePressEvent(QMouseEvent *ev) {
  if (ev->button() != Qt::LeftButton) {
    return;
  }

  auto offsetPos = ev->pos() - QPoint(mExtra.left(), mExtra.top());

  bool oneSelected = false;

  for (auto &r : mRectangles) {
    bool currentSelected =
        !oneSelected && r.mRectangle.mBoundingBox.contains(offsetPos);

    if (setFlagState(r.mFlags, InnerRectangle::k_Selected, currentSelected)) {
      update();
    }
    if (setFlagState(r.mFlags, InnerRectangle::k_Dragging, currentSelected)) {
      update();
    }

    if (currentSelected) {
      startDrag(r, ev->pos());
    }

    oneSelected = oneSelected || currentSelected;
  }
}

void Canvas::mouseReleaseEvent(QMouseEvent *ev) {
  if (ev->button() != Qt::LeftButton) {
    return;
  }
  for (auto &r : mRectangles) {
    setFlagState(r.mFlags, InnerRectangle::k_Dragging, false);
    resolveDrag();
  }
}

void Canvas::startDrag(const InnerRectangle &target, QPoint pos) {
  mRectangleDrag = DragData{pos, target, target};
}

void Canvas::resolveDrag() {
  if (!mRectangleDrag.has_value()) {
    return;
  }

  for (auto &r : mRectangles) {
    if (r.mId == mRectangleDrag->mOriginalRectangle.mId) {
      r = mRectangleDrag->mModifiedRectangle;
      if (setFlagState(r.mFlags, InnerRectangle::k_Dragging, false)) {
        update();
      }
    }
  }

  mRectangleDrag.reset();
}

void Canvas::paintHelperUnderlay(
    QPainter &painter, const QRect &rect,
    const std::vector<Canvas::InnerRectangle> &rectangles,
    const std::optional<Canvas::DragData> &rectangleDrag,
    const SnapMatchData &snapData) {

  if (!rectangleDrag.has_value()) {
    return;
  }
  auto dragRect =
      rectangleDrag.value().mModifiedRectangle.mRectangle.mBoundingBox;

  QRect verticalRect =
      QRect(QPoint(dragRect.left(), 0), QSize(dragRect.width(), rect.height()));
  QRect horizontalRect =
      QRect(QPoint(0, dragRect.top()), QSize(rect.width(), dragRect.height()));
  painter.fillRect(verticalRect, QColor(255, 0, 0, 40));
  painter.fillRect(horizontalRect, QColor(255, 0, 0, 40));
}

void Canvas::paintHelperOverlay(
    QPainter &painter, const QRect &rect,
    const std::vector<Canvas::InnerRectangle> &rectangles,
    const std::optional<Canvas::DragData> &rectangleDrag,
    const SnapMatchData &snapData) {

  if (!rectangleDrag.has_value()) {
    return;
  }

  if (DrawAllCloseGuides) {
    Snapping::drawVerticalSnapList(painter, rect, snapData.mSnapGrid.mVertical,
                                   QColor(30, 30, 30, 150), 2, 5);
    Snapping::drawHorizontalSnapList(painter, rect,
                                     snapData.mSnapGrid.mHorizontal,
                                     QColor(30, 30, 30, 150), 2, 5);
  }

  if (DrawMatchedGuides) {
    if (!snapData.mHorizontal.mExacts.empty()) {
      Snapping::drawHorizontalSnapList(
          painter, rect, snapData.mHorizontal.mExacts, QColor("red"), 3, 5);
    } else if (!snapData.mHorizontal.mFuzzys.empty()) {
      Snapping::drawHorizontalSnapList(
          painter, rect, snapData.mHorizontal.mFuzzys, QColor("red"), 3, 5);
    }

    if (!snapData.mVertical.mExacts.empty()) {
      Snapping::drawVerticalSnapList(painter, rect, snapData.mVertical.mExacts,
                                     QColor("red"), 3, 5);
    } else if (!snapData.mVertical.mFuzzys.empty()) {
      Snapping::drawVerticalSnapList(painter, rect, snapData.mVertical.mFuzzys,
                                     QColor("red"), 3, 5);
    }
  }
}

void Canvas::paintRectangles(
    QPainter &painter, const std::vector<Canvas::InnerRectangle> &rectangles,
    const std::optional<DragData> &rectangleDrag) {
  for (const auto &r : rectangles) {
    if (r.mFlags & InnerRectangle::k_Dragging) {
      assert(rectangleDrag.has_value());
      painter.fillRect(
          rectangleDrag->mModifiedRectangle.mRectangle.mBoundingBox,
          rectangleDrag->mModifiedRectangle.mRectangle.mColor);
      continue;
    }
    painter.fillRect(r.mRectangle.mBoundingBox, r.mRectangle.mColor);
  }
}

void Canvas::paintRectangleOverlays(
    QPainter &painter, const std::vector<InnerRectangle> &rectangles,
    const std::optional<DragData> &rectangleDrag) {
  painter.save();
  painter.setRenderHint(QPainter::Antialiasing, true);

  auto lw = 3.0;
  auto oPen = QPen(QColor(13, 214, 255, 180), lw);
  oPen.setJoinStyle(Qt::RoundJoin);
  auto sPen = QPen(QColor(13, 214, 255, 255), lw);
  sPen.setJoinStyle(Qt::RoundJoin);

  if (rectangleDrag.has_value()) {
    // painter.setPen(sPen);
    // painter.drawRect(rectangleDrag->mModifiedRectangle.mRectangle.mBoundingBox);
    painter.restore();
    return;
  }

  for (const auto &r : rectangles) {
    if (r.mFlags & InnerRectangle::k_Selected) {
      painter.setPen(sPen);
      painter.drawRect(r.mRectangle.mBoundingBox);
    } else if (r.mFlags & InnerRectangle::k_MouseOver) {
      painter.setPen(oPen);
      painter.drawRect(r.mRectangle.mBoundingBox);
    }
  }

  painter.restore();
}

CanvasContainer::CanvasContainer(QWidget *parent) : QScrollArea(parent) {
  mCanvas = new Canvas(this);

  setWidget(mCanvas);
}

CanvasContainer::~CanvasContainer() {}

} // namespace gris
