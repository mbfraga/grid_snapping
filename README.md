# grid_snapping

Exploration into pixel grid snapping

I wrote this in 4 hours, including ideation...so please overlook the mess. Most
of the relevant code is in `gris_Canvas.cpp`.

This is my exploration, having zero experience in snapping. I cam up with the
idea and just wrote it. I'll use this as a playground.

The current implementation runs everything every single drag event, but in
theory a large of it can be pre-calculated on drag start. I tested this with
50000 or more rectangles, and it takes 0-1ms. 

Again, the code isn't ideal:
* Drawing is arguably slow (once you get to 50,000 or so)
* The model is a vector of rectangles--where in reality it's more probable
  to have a more hierarchical one, which would help filtering out bounding 
  boxes

  I played with ideas in my mind with partitioning space--but it's probably
  overkill. It could potentially even have a negative impact. May play around
  with it more if I make the model a bit less dumb.

Todo:

[x] Generate rectangular bounding boxes with ability to move them.

[x] On drag, filter out rectangles that have relevant points of interest.

[x] Calculate ideal point of interests in vertical and horizonal direction.

[x] Separate snapping code into its own utility.

[ ] Improve rectangle model to make it more interesting (multi-select?) and
    closer to a real case.

[ ] Probably a boatload of things I'm missing.



## Instructions

```sh
git clone https://gitlab.com/mbfraga/grid_snapping.git
cd grid_snapping
mkdir build
cd build
cmake ..
make
./grid_snapping
```


